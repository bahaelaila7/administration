Please add your UGA ID, your bitbucket ID, and the paper you wish to present by any available date. Don't worry if the table columns don't line up in the Markdown code.

Here's an example:

Date   | UGA ID | BitBucket ID | Paper title (and link) |
------ | ------ | ------------ | ---------------------- |
1/6    | spq    | magsol       | [CSCI 6900 Lecture 1](http://cobweb.cs.uga.edu/~squinn/mmd_s15/lectures.html)  |

Please sign up here **by Friday, Jan 9.** Anyone not signed up by then will be randomly assigned a presentation date and paper. See the website for a list of paper suggestions. If you choose not to use one of the papers listed, please seek my approval first (I'll most likely say yes).

Date   | UGA ID | BitBucket ID | Paper title (and link) |
------ | ------ | ------------ | ---------------------- |
Jan 13 |        |              |                        |
Jan 15 |        |              |                        |
Jan 20 |        |              |                        |
Jan 22 |        |              |                        |
Jan 27 |        |              |                        |
Feb 3  |bitak   |bitakazemi    | [Distributed Approximate Spectral Clustering for Large-Scale Datasets](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/p223-hefeeda.pdf)                       |
Feb 5  |wdr525  |wdr525        |[Distributed PCA and k-Means Clustering](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/DistributedPCA.pdf)|
Feb 10 |        |              |                        |
Feb 12 |        |              |                        |
Feb 17 |        |              |                        |
Feb 24 |        |              |                        |
Mar 3  |        |              |                        |
Mar 19 |        |              |                        |
Mar 24 |        |              |                        |
Mar 26 |        |              |                        |
Mar 31 |        |              |                        |
Apr 7  |        |              |                        |
Apr 9  |        |              |                        |
